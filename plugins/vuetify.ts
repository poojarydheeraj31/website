// plugins/vuetify.js
import { createVuetify } from "vuetify";

// colors

const dark = {
    variables: {},
    colors: {
        background: "#f8f8f8",
        primary: "#202020",
        secondary: "#797979",
        surface: "#FFFFFF",
        fontsec: "737B8F",
        error: "#B00020",
        button: "#3A44FF",
        info: "#2196F3",
        success: "#4CAF50",
        warning: "#FB8C00",
        footer: "#202020",
    },
};

// export

export default defineNuxtPlugin((nuxtApp) => {
    const vuetify = createVuetify({
        ssr: true,

        theme: {
            defaultTheme: "dark",
            themes: {
                dark,
            },
        },
    });

    nuxtApp.vueApp.use(vuetify);
});
